import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Person } from './model/person';



@Injectable({
  providedIn: 'root'
})
export class AglService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      // tslint:disable-next-line: max-line-length
    })
  };
  constructor(private http: HttpClient) { }

  getPeoplePets(): Observable<object> {
    const url = environment.baseUrl + 'people.json';
    console.log(url);
    return this.http.get(url, this.httpOptions);
  }


}
