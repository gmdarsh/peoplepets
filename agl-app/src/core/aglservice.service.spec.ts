import { TestBed } from '@angular/core/testing';

import { AglService } from './aglservice.service';

describe('AglService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AglService = TestBed.get(AglService);
    expect(service).toBeTruthy();
  });
});
