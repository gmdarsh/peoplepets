import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PeoplepetsComponent } from './peoplepets/peoplepets.component';
import { AglService } from 'src/core/aglservice.service';
import { HttpClient, HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    PeoplepetsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule
  ],
  providers: [AglService],
  bootstrap: [AppComponent]
})
export class AppModule { }
