import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeoplepetsComponent } from './peoplepets.component';

describe('PeoplepetsComponent', () => {
  let component: PeoplepetsComponent;
  let fixture: ComponentFixture<PeoplepetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeoplepetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeoplepetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
