import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { AglService } from 'src/core/aglservice.service';
import { Person } from 'src/core/model/person';
import { Pet } from 'src/core/model/pet';

@Component({
  selector: 'app-peoplepets',
  templateUrl: './peoplepets.component.html',
  styleUrls: ['./peoplepets.component.scss']
})
export class PeoplepetsComponent implements OnInit, OnChanges {

  @Input() People: Array<Person>;
  CatsWithOwnerGender: Map<string, Array<Pet>> = new Map<string, Array<Pet>>();
  genderKeys: IterableIterator<string>;
  constructor(private aglService: AglService) {
  }

  ngOnInit() {
    this.aglService.getPeoplePets()
    .subscribe((res: Array<Person>) => {
      this.People = res;
      this.getListOfAllCatsWithOwnerGender(this.People);
      this.genderKeys = this.CatsWithOwnerGender.keys();
    });
  }

  ngOnChanges() {
    console.log(this.People);
  }

  getSortedCats(key: string): Array<Pet> {
    const pets = this.CatsWithOwnerGender.get(key);
    return pets.sort((n1 , n2) => {
      if (n1.name > n2.name) {
          return 1;
      }

      if (n1.name < n2.name) {
          return -1;
      }
      return 0;
  });
}

  getListOfAllCatsWithOwnerGender(people: Array<Person>) {
    people.forEach(s => {

      if (s.pets) {
        if (s.pets.some(c => c.type.toLowerCase() === 'cat')) {
          const pets = s.pets.filter(c => c.type.toLowerCase() === 'cat');
          if (pets) {
            if (this.CatsWithOwnerGender.has(s.gender)) {
              const values = this.CatsWithOwnerGender.get(s.gender);

              pets.forEach( p => values.push(p));

            } else {
                this.CatsWithOwnerGender.set(s.gender, pets);
            }
          }
        }
      }
    });
  }
}
