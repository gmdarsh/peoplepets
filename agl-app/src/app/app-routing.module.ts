import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeoplepetsComponent } from './peoplepets/peoplepets.component';

const routes: Routes = [
  { path: '**', component: PeoplepetsComponent }
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
